# Documentation

First steps: Set up Jelastic CLI to simplify code deployment.

Jelastic CLI works best on Mac/Linux, on Windows using the Windows Subsystem
for Linux is a usable alternative. Prerequisites:

NodeJS, NPM, Maven

For NodeJS and NPM, using [nvm](https://github.com/nvm-sh/nvm) is the easiest option to get a working NodeJS
environment which is usable with Windows.

Maven can be installed the usual way, it comes with binaries for Windows and
Linux. Just set up the %JAVA_HOME% in the `Path` Environment variables and it
should work.

Next:

- Add the Jelastic Maven Plugin to the `pom.xml` of Spring-Boot applications as
described in the
[documentation](https://docs.jelastic.com/maven-plugin-jelastic).

- Create environment with Springboot Application with the Jelastic GUI, expose Endpoint of application (eg. 8110 for customer-core)

- Build and deploy the application from the CLI using `mvn clean install jelastic:deploy`

For the NodeJS based Applications:

- Add Git Repository for the Frontend Application
- Deploy it to NodeJS Application Server with npm as a process manager.
- Expose Endpoints accordingly

$TODO
Link containers (ENV VARS etc...)
